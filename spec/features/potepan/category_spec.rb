require 'rails_helper'

RSpec.feature 'Category', type: :feature do
  let!(:taxon)          { create(:taxon) }
  let!(:taxonomy)       { create(:taxonomy, name: 'Categories') }
  let(:product)         { create(:product) }
  let!(:classification) { create(:classification, product_id: product.id, taxon_id: taxon.id) }

  describe 'トップページへの遷移が正しく行われる' do
    before do
      visit potepan_category_path(taxon.id)
    end

    scenario 'BrandLogoをクリックしたらトップページに遷移する' do
      click_on(class: 'navbar-brand')
      expect(page).to have_current_path potepan_path
    end

    scenario 'NavbarのHomeリンクをクリックしたらトップページに遷移する' do
      within '.navbar' do
        click_link('Home')
      end
      expect(page).to have_current_path potepan_path
    end

    scenario 'PageHeaderのHomeリンクをクリックしたらトップページに遷移する' do
      within '.pageHeader' do
        click_link('Home')
      end
      expect(page).to have_current_path potepan_path
    end
  end

  describe '別のカテゴリーページへの遷移が正しく行われる' do
    let(:another_taxon)           { create(:taxon, name: 'another_taxon', taxonomy: taxon.taxonomy) }
    let(:another_product)         { create(:product, price: 29.99) }
    let!(:another_classification) { create(:classification, product_id: another_product.id, taxon_id: another_taxon.id) }
    before do
      visit potepan_category_path(taxon.id)
    end

    scenario '商品カテゴリーのリストからリンクをクリックして、別のカテゴリーページが正しく表示される', js: true do
      click_link(taxon.taxonomy.name)
      click_link("#{another_taxon.name}(#{another_taxon.products.length})")
      expect(page).to have_current_path potepan_category_path(another_taxon.id)
      expect(page).to have_text(/#{another_product.name}/i)
      expect(page).to have_text another_product.price
      expect(page).not_to have_text(/#{product.name}/i)
      expect(page).not_to have_text product.price
    end
  end

  describe '商品詳細ページへの遷移が正しく行われる' do
    before do
      visit potepan_category_path(taxon.id)
    end

    scenario '商品をクリックしたら、商品詳細ページに遷移する' do
      click_link(product.name)
      expect(page).to have_current_path potepan_product_path(product.id)
    end
  end
end
