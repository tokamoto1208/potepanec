require 'rails_helper'

RSpec.feature 'SingleProduct', type: :feature do
  let(:product)         { create(:product, price: 19.99) }
  let(:image)           { create(:image, viewable_id: product.master.id) }
  let(:images)          { create_list(:image, 2, viewable_id: product.master.id) }
  let(:taxonomy)        { create(:taxonomy, name: 'Categories') }
  let!(:classification) { create(:classification, product_id: product.id, taxon_id: taxonomy.root.id) }

  before do
    visit potepan_product_path(product.id)
  end

  describe 'トップページへの遷移' do
    scenario 'BrandLogoをクリックしたらトップページに遷移する' do
      click_on(class: 'navbar-brand')
      expect(page).to have_current_path potepan_path
    end

    scenario 'NavbarのHomeリンクをクリックしたらトップページに遷移する' do
      within '.navbar' do
        click_link('Home')
      end
      expect(page).to have_current_path potepan_path
    end

    scenario 'LightSectionのHomeリンクをクリックしたらトップページに遷移する' do
      within '.lightSection' do
        click_link('Home')
      end
      expect(page).to have_current_path potepan_path
    end
  end

  describe 'カテゴリーページへの遷移' do
    scenario '一覧ページへ戻るリンクをクリックしたらカテゴリーページに遷移する' do
      click_link('一覧ページへ戻る')
      expect(page).to have_current_path potepan_category_path(taxonomy.root.id)
    end
  end

  describe '商品画像の初期表示が正しく行われる' do
    context 'MasterVariantの画像が複数ある' do
      it 'メイン画像1枚、全てのサムネイルが表示される' do
        # associationで全モデルを作成したかったが、以下の理由で断念した。
        # solidusの機能ではproduct作成時に必ずmaster_variantが作成される。
        # master_variantのassociationにproductを指定した場合には、先にproduct、および、product.masterの作成が行われる。
        # その後、こちらで指定したmaster_variantが作成され、productに紐付けしにいくが、既にproduct.masterが存在するので、重複エラーになる。
        product.master.images = images
        visit potepan_product_path(product.id)
        within '.productSlider' do
          expect(page.all('.item').length).to eq 2
          expect(page.all('.active').length).to eq 1
          expect(page.all('.thumb').length).to eq 2
        end
      end
    end

    context 'MasterVariantの画像が1つ' do
      it 'メイン画像とサムネイルが1枚ずつ表示される' do
        product.master.images = [image]
        visit potepan_product_path(product.id)
        within '.productSlider' do
          expect(page.all('.item').length).to eq 1
          expect(page.all('.active').length).to eq 1
          expect(page.all('.thumb').length).to eq 1
        end
      end
    end

    context 'MasterVariantの画像がない' do
      it 'デフォルト画像が表示、サムネイルは表示されない' do
        visit potepan_product_path(product.id)
        within '.productSlider' do
          expect(page.all('.item').length).to eq 0
          expect(page.all('.active').length).to eq 0
          expect(page.all('.thumb').length).to eq 0
          expect(page.all('.default_image').length).to eq 1
        end
      end
    end
  end

  describe '商品画像の切り替えが正しく行われる' do
    scenario 'サムネイルをクリックしたらメイン画像が切り替わる', js: true do
      product.master.images = images
      visit potepan_product_path(product.id)
      within '.productSlider' do
        # クリックした後、画像が切り替わるまでの時間が0.6秒かかる
        # テスト環境のみ、CSSのTransitionやJSのエフェクトをオフにする対応を試してみたが、即時切り替えにすることが出来なかった
        # おそらくJSのDelayが効いていることが理由だと思うが、これをオフにすることが出来ず、今回はSleepで0.6秒待つことにした
        within page.all('.thumb')[1] do
          page.find('img').click
        end
        sleep 0.6
        expect(page.find('.active').value).to eq 'image1'

        # 一度メイン画像を切り替えた後で、別のサムネイルをクリックしても正しく切り替わるか
        within page.all('.thumb')[0] do
          page.find('img').click
        end
        sleep 0.6
        expect(page.find('.active').value).to eq 'image0'
      end
    end
  end

  describe '関連商品の表示' do
    context '関連商品がある場合' do
      let(:relative_product)                 { create(:product, price: 29.99) }
      let!(:relative_product_classification) { create(:classification, product_id: relative_product.id, taxon_id: taxonomy.root.id) }
      before do
        visit potepan_product_path(product.id)
      end

      it '関連商品が表示される' do
        expect(page).to have_text '関連商品'
        expect(page).to have_text(/#{relative_product.name}/i)
        expect(page).to have_text relative_product.price
      end

      it '関連商品をクリックしたら関連商品のページに遷移する' do
        click_link(relative_product.name)
        expect(page).to have_current_path potepan_product_path(relative_product.id)
        within '.singleProduct' do
          expect(page).to have_text(/#{relative_product.name}/i)
          expect(page).to have_text relative_product.price
          expect(page).not_to have_text(/#{product.name}/i)
          expect(page).not_to have_text product.price
        end
      end

      context '関連商品が5件以上ある場合' do
        let(:relative_products) { create_list(:product, 5) }
        before do
          relative_products.each do |relative_product|
            create(:classification, product_id: relative_product.id, taxon_id: taxonomy.root.id)
          end
          visit potepan_product_path(product.id)
        end

        it '関連商品が4件表示される' do
          within '.productsContent' do
            expect(page.all('.productBox').length).to eq 4
          end
        end
      end
    end

    context '関連商品がない場合' do
      it '関連商品が表示されない' do
        expect(page).not_to have_text '関連商品'
        expect(page.all('.productContent').length).to eq 0
      end
    end
  end
end
