require 'rails_helper'

RSpec.describe 'Potepan::Categoriess', type: :request do
  describe '#show' do
    include ActionController::Helpers
    include Spree::BaseHelper
    include Spree::Core::ControllerHelpers::Store
    include Spree::Core::ControllerHelpers::Pricing

    let!(:taxon)          { create(:taxon) }
    let!(:classification) { create(:classification, product_id: product.id, taxon_id: taxon.id) }
    let(:product)         { create(:product) }

    before do
      get potepan_category_path(taxon.id)
    end

    it 'リクエストが成功する' do
      expect(response).to be_successful
    end

    it 'タイトルが正しい' do
      expect(response.body).to have_tag('title', text: "#{taxon.name} - BIGBAG Store")
    end

    it 'PageHeaderにTaxon名が表示される' do
      expect(response.body).to have_tag('h2', text: taxon.name)
      expect(response.body).to have_tag('li', text: taxon.name)
    end

    it 'PageHeaderにShopへのリンクが表示される' do
      expect(response.body).to have_tag('a', text: 'shop')
    end

    describe '商品カテゴリーの表示' do
      describe 'Taxonomyの表示' do
        it '表示される' do
          expect(response.body).to have_tag('a', text: taxon.taxonomy.name)
        end
      end

      describe 'Taxonの表示' do
        let(:custom_taxonomy)     { create(:taxonomy, name: 'custom_taxonomy') }
        let(:custom_parent_taxon) { create(:taxon, name: 'custom_parent_taxon', taxonomy: custom_taxonomy) }
        let!(:custom_child_taxon) { create(:taxon, name: 'custom_child_taxon', taxonomy: custom_taxonomy, parent_id: custom_parent_taxon.id) }
        before do
          get potepan_category_path(taxon.id)
        end

        context '子を持たないTaxon' do
          it '表示される' do
            # link_toをブロックで実装しているとhtmlソースに改行が入るためか、aタグでマッチさせることが出来なかった
            expect(response.body).to include("#{custom_child_taxon.name}<span>(#{custom_child_taxon.products.length})</span>")
          end
        end

        context '子を持つTaxon' do
          it '表示されない' do
            expect(response.body).not_to include("#{custom_parent_taxon.name}<span>(#{custom_parent_taxon.products.length})</span>")
          end
        end
      end
    end

    describe '商品の表示' do
      context '子を持たないTaxon' do
        it '自分に紐づく商品の名前が表示される' do
          expect(response.body).to have_tag('h5', text: product.name)
        end

        it '自分に紐づく商品の値段が表示される' do
          expect(response.body).to have_tag('h3', text: display_price(product))
        end
      end

      context '子を持つTaxon' do
        let(:child_taxon)                 { create(:taxon, parent_id: taxon.id) }
        let(:child_taxon_product)         { create(:product, price: 29.99) }
        let!(:child_taxon_classification) { create(:classification, product_id: child_taxon_product.id, taxon_id: child_taxon.id) }
        before do
          get potepan_category_path(taxon.id)
        end
        it '子に紐づく商品の名前が表示される' do
          expect(response.body).to have_tag('h5', text: child_taxon_product.name)
        end

        it '子に紐づく商品の値段が表示される' do
          expect(response.body).to have_tag('h3', text: display_price(child_taxon_product))
        end
      end
    end
  end
end
