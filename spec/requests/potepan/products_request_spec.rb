require 'rails_helper'

RSpec.describe 'Potepan::Products', type: :request do
  describe '#show' do
    include ActionController::Helpers
    include Spree::BaseHelper
    include Spree::Core::ControllerHelpers::Store
    include Spree::Core::ControllerHelpers::Pricing

    let(:product)           { create(:product, price: 19.99) }
    let(:relative_product)  { create(:product, price: 29.99) }
    let(:taxonomy)          { create(:taxonomy, name: 'Categories') }
    let!(:classification_1) { create(:classification, product_id: product.id, taxon_id: taxonomy.root.id) }
    let!(:classification_2) { create(:classification, product_id: relative_product.id, taxon_id: taxonomy.root.id) }

    before do
      get potepan_product_path(product.id)
    end

    it 'リクエストが成功する' do
      expect(response).to be_successful
    end

    it 'テンプレートが正しく表示される' do
      expect(response).to render_template :show
    end

    it 'タイトルが正しい' do
      expect(response.body).to have_tag('title', text: "#{product.name} - BIGBAG Store")
    end

    it 'PageHeaderにShopへのリンクが表示されない' do
      expect(response.body).not_to have_tag('a', text: 'shop')
    end

    it '商品名が表示される' do
      expect(response.body).to have_tag('h2', text: product.name)
    end

    it '商品の値段が表示される' do
      expect(response.body).to have_tag('h3', text: display_price(product))
    end

    it '商品の説明が表示される' do
      expect(response.body).to have_tag('p', text: product.description)
    end

    it '関連商品の商品名が表示される' do
      expect(response.body).to have_tag('h5', text: relative_product.name)
    end

    it '関連商品の値段が表示される' do
      expect(response.body).to have_tag('h3', text: display_price(relative_product))
    end
  end
end
