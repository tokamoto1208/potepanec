require 'rails_helper'

RSpec.describe Potepan::ProductDecorator, type: :decorator do
  let!(:product)  { create(:product, taxons: [taxonomy1.root, taxonomy2.root]) }
  let(:taxonomy1) { create(:taxonomy, name: 'taxonomy1') }
  let(:taxonomy2) { create(:taxonomy, name: 'taxonomy2') }

  describe 'taxon_in_taxonomy' do
    it 'Productが持っているTaxonの中から指定のTaxonomyに紐づくTaxonを返す' do
      expect(product.taxon_in_taxonomy(taxonomy1.name)).to eq product.taxons[0]
    end

    context 'Productが指定のTaxonomyに紐づくTaxonを持っていない場合' do
      let!(:taxonomy3) { create(:taxonomy, name: 'taxonomy3') }

      it 'nilを返す' do
        expect(product.taxon_in_taxonomy(taxonomy3.name)).to eq nil
      end
    end

    context '引数にnilが渡された場合' do
      it 'nilを返す' do
        expect(product.taxon_in_taxonomy(nil)).to eq nil
      end
    end
  end

  describe 'relative_products' do
    context '関連するProductが発売日を迎えている場合' do
      let(:relative_product_old) { create(:product, available_on: Time.current.yesterday) }
      let(:relative_product_new) { create(:product, available_on: Time.current) }
      let!(:classification1)     { create(:classification, product_id: relative_product_old.id, taxon_id: taxonomy1.root.id) }
      let!(:classification2)     { create(:classification, product_id: relative_product_new.id, taxon_id: taxonomy1.root.id) }

      it '関連するProductを発売日順のListで返す' do
        expect(product.relative_products(2)).to eq [relative_product_new, relative_product_old]
      end
    end

    context '関連するProductが発売日を迎えていない場合' do
      let(:relative_product_not_available) { create(:product, available_on: Time.current.tomorrow) }
      let!(:classification3) { create(:classification, product_id: relative_product_not_available, taxon_id: taxonomy1.root.id) }

      it '発売日を迎えていないProductは返さない' do
        expect(product.relative_products(1)).to eq []
      end
    end

    context '1つの別のProductが、複数のTaxonから関連していて、重複した関連を持つ場合' do
      let(:relative_product_some_taxon) { create(:product) }
      let!(:classification4) { create(:classification, product_id: relative_product_some_taxon.id, taxon_id: taxonomy1.root.id) }
      let!(:classification5) { create(:classification, product_id: relative_product_some_taxon.id, taxon_id: taxonomy2.root.id) }

      it '重複した関連を持っていても、返ってくるListの要素に同じProductは重複して含まれない' do
        expect(product.relative_products(2)).to eq [relative_product_some_taxon]
      end
    end

    context '関連するProductが存在しない場合' do
      it '空の配列を返す' do
        expect(product.relative_products(1)).to eq []
      end
    end

    context '引数にnilが渡された場合' do
      it '空の配列を返す' do
        expect(product.relative_products(nil)).to eq []
      end
    end
  end
end
