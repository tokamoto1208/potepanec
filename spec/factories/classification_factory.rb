FactoryBot.define do
  factory :classification, class: 'Spree::Classification' do
    product_id { 1 }
    taxon_id   { 1 }
  end
end
