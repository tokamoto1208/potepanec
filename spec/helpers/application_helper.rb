require 'rails_helper'

RSpec.describe ApplicationHelper do
  include ApplicationHelper
  describe 'title' do
    it { expect(title('Potepan')).to eq 'Potepan - BIGBAG Store' }
    it { expect(title('')).to eq 'BIGBAG Store' }
    it { expect(title(nil)).to eq 'BIGBAG Store' }
    it { expect(title).to eq 'BIGBAG Store' }
  end
end
