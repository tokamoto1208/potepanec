require 'rails_helper'

RSpec.describe Potepan::CategoriesHelper, type: :helper do
  let!(:taxonomy) { create(:taxonomy, name: 'Categories') }

  describe 'root_taxon_in_taxonomy' do
    it 'taxonomyのroot_taxonを返す' do
      expect(root_taxon_in_taxonomy(taxonomy.name)).to eq taxonomy.root
    end

    context '引数にnilが渡された場合' do
      it 'nilを返す' do
        expect(root_taxon_in_taxonomy(nil)).to eq nil
      end
    end
  end
end
