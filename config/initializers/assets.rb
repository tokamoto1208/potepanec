# Be sure to restart your server when you modify this file.

# Version of your assets, change this if you want to expire all your assets.
Rails.application.config.assets.version = '1.0'

# Add additional assets to the asset load path.
# Rails.application.config.assets.paths << Emoji.images_path
# Add Yarn node_modules folder to the asset load path.
# Rails.application.config.assets.paths << Rails.root.join('node_modules')

#JSを有効にしたテスト環境で以下のファイルを読み込むことが出来なかったので追加
Rails.application.config.assets.paths << Rails.root.join("app", "assets", "images", "plugins", "owl-carousel")
Rails.application.config.assets.paths << Rails.root.join("app", "assets", "images", "plugins", "rs-plugin", "assets")
Rails.application.config.assets.paths << Rails.root.join("app", "assets", "stylesheets", "plugins", "selectbox")

# Precompile additional assets.
# application.js, application.css, and all non-JS/CSS in the app/assets
# folder are already added.
# Rails.application.config.assets.precompile += %w( admin.js admin.css )
Rails.application.config.assets.precompile += %w( disable_animation.css )
