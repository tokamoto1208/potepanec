module Potepan
  class ProductsController < ApplicationController
    def show
      @product = Spree::Product.find(params[:id])
      @relative_products = @product.relative_products(
        Constants::MAX_RELATIVE_PRODUCTS_NUM
      ).includes(master: %i[images currently_valid_prices])
    end
  end
end
