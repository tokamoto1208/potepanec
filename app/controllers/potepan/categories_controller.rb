module Potepan
  class CategoriesController < ApplicationController
    def show
      @taxon = Spree::Taxon.find(params[:id])
      @taxonomies = Spree::Taxonomy.all.includes(taxons: %i[children products])
      @products = @taxon.all_products.includes(master: %i[images currently_valid_prices])
    end
  end
end
