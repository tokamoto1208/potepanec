module Potepan
  module ProductDecorator
    def self.prepended(base)
      base.scope :descend_by_available, -> { order(available_on: :desc) }
    end

    def taxon_in_taxonomy(name)
      # Solidusの仕様としては、同じTaxonomyの中でも複数のTaxonに商品を登録することが出来る
      # ただし実在のECサイトを考えてみても、そういったカテゴリの登録の仕方はしないと思うので、運用ルールでカバーするべき問題だと考えた
      # 念のためlimitで明示的に1レコードしか取得しないようにした
      Spree::Taxon.joins(:products, :taxonomy).where(
        spree_products: { id: id }, spree_taxonomies: { name: name }
      ).limit(1)[0]
    end

    def relative_products(limit)
      Spree::Product.in_taxons(taxons).distinct.where.not(id: id).available.descend_by_available.limit(limit)
    end

    Spree::Product.prepend self
  end
end
