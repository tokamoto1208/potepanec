module Potepan
  module CategoriesHelper
    def root_taxon_in_taxonomy(name)
      return if name.blank?

      Spree::Taxonomy.find_by(name: name).root
    end
  end
end
