module ApplicationHelper
  include Spree::BaseHelper
  def title(title = '')
    if title.blank?
      'BIGBAG Store'
    else
      "#{title} - BIGBAG Store"
    end
  end
end
